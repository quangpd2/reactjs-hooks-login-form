import {Button, Container,Grid,Link,TextField,Typography} from "@mui/material"
import { useState } from "react"
const LoginForm = () =>{

    const [emailAddress,setEmailAddress] = useState("");
    const [passWord,setpassWord] = useState("");

    const onEmailAddressChange = (event) =>{
        setEmailAddress(event.target.value);
    }

    const onPassWordChange = (event) =>{
        setpassWord(event.target.value);
    }
   

    return(
        <>
             <Container>
                <Grid container mt={2} >
                    <Grid item xs={12} sm={12} md={12} lg={12} >
                        <Typography color={"#faf9f1"} variant="h4">
                            Welcome Back!
                        </Typography>
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={12} mt={3} >
                        <TextField id="outlined-basic" label="Email Address" sx={{backgroundColor : "white"}} variant="outlined" color="secondary" onChange={onEmailAddressChange} fullWidth value={emailAddress} />
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={12} mt={3} >
                        <TextField id="outlined-basic" label="Password" sx={{backgroundColor : "white"}} onChange={onPassWordChange} variant="outlined" fullWidth value={passWord} />
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={12} textAlign={"end"}>
                        {/* <Typography color={"#207a66"} variant="caption">Forgot Password?</Typography> */}
                        <Link href="#" color={"#207a66"} underline="none">Forgot Password?</Link>
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={12} mt={2} >
                        <Button sx={{height : "60px"}} color="success" fullWidth variant="contained">Đăng Nhập</Button>
                    </Grid>
                </Grid>
            </Container>
        </>
    )
}

export default LoginForm