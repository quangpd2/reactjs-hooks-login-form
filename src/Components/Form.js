import {Container,Grid,ButtonGroup,Button} from "@mui/material" 
import { useState } from "react"
import SignUpForm from "./SignUpForm";
import LoginForm from "./LoginForm";

const Form = () =>{

    const [isLogin , setIsLogin] = useState(true);

    const onSignUpClick = () =>{
        setIsLogin(false);
    }

    const onLoginClick = () =>{
        setIsLogin(true);
    }
    return(
        <>
            <Container >
                <Grid container mt={5} justifyContent={"center"} height={"50vh"}>
                    <Grid item xs={12} sm={12} md={8} lg={6}  sx={{backgroundColor : "#c0bcb9"}} textAlign={"center"}  >
                        <ButtonGroup color="success" variant="contained" fullWidth aria-label="outlined primary button group">
                            <Button color={isLogin ? "inherit" : "success"} onClick={onSignUpClick} > Sign Up </Button>
                            <Button color={isLogin ? "success" : "inherit"}onClick={onLoginClick}> Login</Button>
                        </ButtonGroup>
                        {
                            isLogin ? <LoginForm/> : <SignUpForm/>
                        }
                    </Grid>
                </Grid>

            </Container>
        </>
    )
}

export default Form