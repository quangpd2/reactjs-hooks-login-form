import {Button, Container,Grid,TextField,Typography} from "@mui/material"
import { useState } from "react"

const SignUpForm = () =>{
    const [firstName,setFirstName] = useState("");
    const [lastName,setLastName] = useState("");
    const [emailAddress,setEmailAddress] = useState("");
    const [passWord,setPassword] = useState("");

    const onChangeFirstName = (event) =>{
        setFirstName(event.target.value);
    }

    const onChangeLastName = (event) =>{
        setLastName(event.target.value);
    }

    const onChangeEmail = (event) =>{
        setEmailAddress(event.target.value);
    }

    const onChangePassWord = (event) =>{
        setPassword(event.target.value);
    }


    return(
        <>
            <Container>
                <Grid container mt={2}>
                    <Grid item sm={12} xs={12} md={12} lg={12}>
                        <Typography variant="h4"> Sign Up for Free</Typography>
                    </Grid>
                    <Grid item sm={6} xs={6} md={6} lg={6} mt={2} paddingRight={1}>
                        <TextField sx={{backgroundColor : "white"}} variant="outlined" label="First Name *" value={firstName} onChange={onChangeFirstName} fullWidth/>
                    </Grid>
                    <Grid item sm={6} xs={6} md={6} lg={6} mt={2} paddingLeft={1}>
                        <TextField sx={{backgroundColor : "white"}} variant="outlined" label="Last Name *" value={lastName} onChange={onChangeLastName} fullWidth/>
                    </Grid>
                    <Grid item sm={12} xs={12} md={12} lg={12} mt={2} >
                        <TextField sx={{backgroundColor : "white"}} variant="outlined" label="Email Address *" value={emailAddress} onChange={onChangeEmail} fullWidth/>
                    </Grid>
                    <Grid item sm={12} xs={12} md={12} lg={12} mt={2} >
                        <TextField sx={{backgroundColor : "white"}} variant="outlined" label="Set A Password *" value={passWord} onChange={onChangePassWord} fullWidth/>
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={12} mt={3} >
                        <Button color="success" sx={{height:"60px"}} variant="contained" fullWidth>GET STARTED</Button>
                    </Grid>
                </Grid>
            </Container>
        </>
    )
}

export default SignUpForm